# Dotfiles and maintenance scripts
Not a lot of files because everything else is managed using GNU Emacs.
As a result, my dotfiles are between the Emacs repo and this one.

## Preview
![](img/busy.png)

