# tabbing with insensitive case
autoload -Uz compinit
compinit
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' \
  '+l:|?=** r:|?=**'

# load sway and variables export
export XKB_DEFAULT_LAYOUT=us,ca
export XKB_DEFAULT_OPTIONS=grp:shifts_toggle
# if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
#    sway
#    exit 0
# fi
# export SWAYSOCK=$(ls /run/user/*/sway-ipc.*.sock | head -n 1)

# mail queue path
export QUEUEDIR="$HOME/.emacs.d/mail/queue"

# libreoffice theme
export SAL_USE_VCLPLUGIN=gtk

# npm/node
PATH="$HOME/.node_modules/bin:$PATH"
export npm_config_prefix=~/.node_modules

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH

#export browser and apps
export BROWSER='firefox'

#export the best editor man-made
export EDITOR='emacs'

# Ruby
export GEM_HOME=$HOME/.gem
PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"

# etcher
export PATH="$PATH:/opt/etcher-cli"

# gpg
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
export GPG_TTY=$(tty)
gpg-connect-agent updatestartuptty /bye >/dev/null

# gpodder and download location
GPODDER_HOME="$HOME/media/audio/gpodder/"
GPODDER_DOWNLOAD_DIR="$HOME/media/audio/podcast/"
export GPODDER_HOME
export GPODDER_DOWNLOAD_DIR

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="false"

# Uncomment the following line to disable colors in ls.
DISABLE_LS_COLORS="false"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# User configuration
export MANPATH="/usr/local/man:$MANPATH"

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

alias sysnet_up='sudo netctl start'
alias sysnet_down='sudo netctl stop-all'
alias clip='sh $HOME/dotfiles/scripts/wl_screenshot'

extract() {
   if [ -f $1 ] ; then
       case $1 in
           *.tar.bz2)	tar xvjf $1    ;;
           *.tar.gz)	tar xvzf $1    ;;
	   *.tar.xz)	tar xJf $1     ;;
           *.bz2)	bunzip2 $1     ;;
           *.rar)	unrar x $1     ;;
           *.gz)	gunzip $1      ;;
           *.tar)	tar xvf $1     ;;
           *.tbz2)	tar xvjf $1    ;;
           *.tgz)	tar xvzf $1    ;;
           *.zip)	unzip $1       ;;
           *.Z)		uncompress $1  ;;
           *.7z)	7z x $1        ;;
           *)		echo "Unable to extract '$1'" ;;
       esac
   else
       echo "'$1' is not a valid file"
   fi
}

HISTFILE=$HOME/documents/docs/.histfile
HISTSIZE=500
SAVEHIST=500

[[ -n "${key[Up]}"   ]] && bindkey "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey "${key[Down]}" down-line-or-beginning-search
